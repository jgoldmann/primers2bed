
# primers2bed  

A script to take a list of primers, map them to a specific genome and return a bedfile with the primer's genomic coordinates.

### requirements

- bwa version 0.6.2. or higher
- bedtools suite
- pyhton library pandas


### input data

The primer list is taken from an excel file `snapshot_primerDB.xlsx`, spreadsheat `All_primers_at_NCMLS`. There columns should have the following names:

- `Primer number`: internal primer id
- `Researcher Name`: (optional) name of the person who ordered the primer
- `gene/fragment`: (optional) a string to name the primer
- `Sequence`: the nucleotide sequence of the primer
- `Organism`: (optional) the organism for which the primer was ordered


### species specific data

The species-specific data is hardcoded at the moment, it contains human and mouse variables. You probably want to adapt the location of the bwa indexes. There are the following fields:

- `name`: name of the species
- `bwa_index`: path to the bwa index file
- `genome`: genome assembly
- `fastq`, `bam`, `bed`: filenames of the respective file formats


