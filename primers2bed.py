import subprocess
import os
import pandas as pd

# some test data #
test_entry = {"Primer number":   53157,
              "Researcher Name": "Yiwei",
              "gene/fragment":   "Irf6_Prom_BfaI4CI0013",
              "Sequence":        "GCAAGAAATATGACACTCCG",
              "Organism":        "human",}

# import data #
df = pd.read_excel('snapshot_primerDB.xlsx',
                           'All_primers_at_NCMLS',
                           na_values=['NA'])

assert(len(df[df.Organism=="Human"]) + #make sure there is only those two species
       len(df[df.Organism=="Mouse"]) 
       == len(df[df.Organism.notnull()]))


# species specific data #
human = {'name':  'human',
         'fastq': "human.fq",
         'bam':   "human.bam",
         'bed':   "human.bed",
         'bwa_index': "/home/jakob/data/hg19bwa/hg19.fa",
         'primers':   df[(df.Organism.isnull())|(df.Organism=="Human")],
         'genome':    'hg19'}

mouse = {'name':  'mouse',
         'fastq': "mouse.fq",
         'bam':   "mouse.bam",
         'bed':   "mouse.bed",
         'bwa_index': "/home/jakob/data/mm9bwa/mm9.fa",
         'primers':   df[(df.Organism.isnull())|(df.Organism=="Mouse")],
         'genome':    'mm9'}


# functions #
def convert2fastq(primer, fastq_path):
   if (os.path.exists(fastq_path)): 
       os.unlink(fastq_path)
   fq = open(fastq_path, "a")
   if (type(primer) is dict):
       write_fastq_entry(primer, fq)
   elif (type(primer) is pd.core.frame.DataFrame):
       primer.apply(lambda x: write_fastq_entry(x, fq),1)
   else:
       return 1
   fq.close()
   return 0

def write_fastq_entry(primer, fq):
    if (type(primer["Sequence"]) is unicode):
        fq.write("@" + str(primer["Primer number"]) + "\n")
        fq.write(primer["Sequence"] + "\n")
        fq.write("+" + str(primer["Primer number"]) + " " + 
                 str(primer["gene/fragment"]) + " (" + 
                 str(primer["Researcher Name"]) + ")\n")
        fq.write("J" * len(primer["Sequence"]) + "\n")
    

def align(bwa_index, fastq, bam):
   assert(os.path.exists(bwa_index))
   align_command = "bwa aln -t 4 {0} {1} | bwa samse {0} - {1} | samtools view -Sb - -o {2}".format(bwa_index, fastq, bam)
   alignment = subprocess.Popen(align_command, shell=True)
   if(alignment.wait() == 0):
      print "Successfully mapped the primers to the genome.\n"
      return 0
   else:
      print "Something went wrong during the mapping process :(\n"
      return 1
      
def write_bed_header(species):
    bed = open(species["bed"], "w+")
    bed_header = 'track name="{0} primer stocks" description="primers" visibility=2 db={1}\n'.format(species['name'], species['genome'])
    bed.write(bed_header)
    bed.close()


def visualize(bam, bed):
   visualization_command = "bamToBed -i {0} >> {1}".format(bam, bed)
   visualization = subprocess.Popen(visualization_command, shell=True)
   if(visualization.wait() == 0):
      print "Successfully created .bed file.\n"
      return 0
   else:
      print "Something went wrong during the bedfile visualization:(\n"
      return 1


# routine #
for species in [human, mouse]:
    if (convert2fastq(species['primers'], species["fastq"])==0):
        print "Generated fastq file.\n"
        go_on = True
    else:
        print "Something went wrong during the fastq conversion :(\n"
        go_on = False
            
    if(go_on):
        go_on = not align(species['bwa_index'], species["fastq"], species["bam"])
                
    if(go_on):
        write_bed_header(species)
        visualize(species["bam"], species["bed"])

